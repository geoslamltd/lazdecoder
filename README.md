# LazDecoder #

LazDecoder is an ES6 Module using WebAssembly for decoding LAZ files.
It makes use of laz-perf (https://github.com/hobu/laz-perf) by Howard Butler.

### Building ###

* install the emscripten SDK (activate 1.40.1)
* obtain a copy of laz-perf, checkout tag 1.4.4, and create a link to the laz-perf source subdir such that /src-c/laz-perf contains the io.hpp, factory.hpp, etc. of the laz-perf repo.
* make sure you have cmake installed - see /src-c/CMakeLists.txt for version requirements
* obtain a version of GNU make for win32 and ensure it is found on the path
* edit /src-c/build-stage1.bat to suit your environment
* run /src-c/build-stage1.bat

## Usage ##

* when using the emscripten module in /dist/lazdecode.js provide handlers in Module.superDynamicCallback1 for the actions produced by the various "witness" callbacks of prelink.js.

## LICENSE ##

See COPYING
