/**
 * LazDecoder - Point cloud file decoding for LAZ format.
 *
 * Copyright (C) 2020 Michael Erskine (michael.erskine@geoslam.com)
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * LazDecoder - Point cloud file decoding for LAZ format (with the intention to
 * extend to include LAS and perhaps others).
 *
 * Designed to be used with the emscripten SDK to compile to WebAssembly.
 */
#include <stdio.h>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <memory>
#include <cmath>

#include "io.hpp"
#include "common/common.hpp"
#include "./pointfield.hpp"
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#define say(args...)  emscripten_log(EM_LOG_CONSOLE, args)
#else
#define EMSCRIPTEN_KEEPALIVE
#define say printf
#endif

#undef CSVDUMP

extern "C" uint32_t witnessString(int action, const char* text);
extern "C" uint32_t witnessFloat(int action, double v1);
extern "C" uint32_t witnessPointer(int action, const uint8_t* ptr, uint32_t len);

enum actions {
  ACT_NONE = 0,
  ACT_HEADER = 1,
  ACT_BLOCK_ADDRESS = 2,
  ACT_FINAL_DURATION = 3,
  ACT_POINT_COUNT = 4,
  ACT_GEOM_DEF = 5,
  ACT_OPEN_ERROR = 6,
  ACT_FINAL_CODE = 7,
  ACT_WARNING_MSG = 8,
  ACT_TIME_RANGES_ETC = 9,
};

using namespace std;

struct yikes : public runtime_error {
  yikes(const char *msg) : runtime_error(msg) { }
};

// Simplest packing strategy to begin with - interleaved buffer for WebGL
// X,Y,Z,TIME,R,G,B,I
// 24 bit colour is plenty and the intensity from Horizon is already 8-bit expanded to 16-bit for LAS
// The fields seem to need to be 32-bit aligned - see VizServer code
GeomDef gd("laztype1", "x: FLOAT32, y: FLOAT32, z: FLOAT32, t: FLOAT32, r: UINT8, g: UINT8, b: UINT8, i: UINT8");

/**
 * We need an object that can live outside the "main" loop and safely hold scope for resources that would normally be on the stack
 * in main()
 */
struct CloudFileDecoder {
  unique_ptr<laszip::io::reader::file> lazFileReader;
  unique_ptr<ifstream> inStrm;
  #ifdef CSVDUMP
  ofstream outCsv;
  #endif
  // Buffer on stack that is large enough to hold any reasonable LAS point
  char buf[256];
  // Count of points in file as reported by the header
  size_t pointCount = 0;
  // Index or ID of point in file
  size_t pointId = 0;
  // Index or ID of subsampled/decimated/filtered point that is output to blocks
  size_t pointSampleId = 0;
  uint32_t pointsPerBlock = 50000;
  uint32_t blocksPerYield = 1;
  uint32_t pointBudget = numeric_limits<uint32_t>::max();
  uint32_t blockSizeBytes;
  uint32_t cancelLazLoad = 0;
  // Point budget subsampling/decimation control: zero means we are within the
  // budget. When subsampling, the value will be greater than 1.0...
  double pointBudgetInterval = 0.0;
  uint8_t *bp = nullptr;
  laszip::io::header h;
  int schemaFormat = 0;
  int finalCode = -666;
  int colourShift = 0;
  uint32_t fx, fy, fz, ft, fr, fg, fb, fi;
  double timeZero = 0.0;
  double timeFinal = 0.0;
  bool hasIntensity = false;
  bool hasColour = false;
  // Prior to sending first block of data we might want to inform the caller of some stats...
  bool firstBlockCheck = true;
  chrono::steady_clock::time_point start;

  int openLaz(const char* filename, int ppb, int rgbw, int bpy, int pointBudgetIn) {
    if(ppb > 0) { pointsPerBlock = ppb; }
    if(rgbw == 16) { colourShift = 8; }
    if(bpy > 0) { blocksPerYield = bpy; }
    if(pointBudgetIn > 0) { pointBudget = pointBudgetIn; }
    stringstream ss;
    inStrm = unique_ptr<ifstream>(new ifstream(filename, ios::binary));
    if (!inStrm->good()) {
      ss << "Could not open file for reading: '" << filename << "'";
      if(errno) ss << endl << strerror(errno) << endl;
      witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
      finalCode = -2;
      return finalCode;
    }
    say("File opened OK\n");
    // Here we attempt to read the LAS header and do a bit of checking
    // to avoid running into some exceptions that would be thrown (badly)
    // if the wrong type of file is opened.
    inStrm->read((char*)&h, sizeof(h));
    if (!inStrm->good()) {
      ss << "Could not read LAS/LAZ header: '" << filename << "'";
      if(errno) ss << endl << strerror(errno) << endl;
      witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
      finalCode = -4;
      return finalCode;
    }
    // for(int i = 0; i < 4; i++){
    //   ss << "char["<<i<<"] = '"<< (int)h.magic[i] << "'\n";
    // }
    // say(ss.str().c_str());
    if (std::string(h.magic, 4) != "LASF") {
      ss.str("");
      ss << "Incorrect LAS/LAZ header: '" << filename << "'";
      witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
      finalCode = -5;
      return finalCode;
    }
    {
      int bit_7 = (h.point_format_id >> 7) & 1;
      int bit_6 = (h.point_format_id >> 6) & 1;
      if (bit_7 == 1 && bit_6 == 1) {
        ss.str("");
        ss << "Old style LAZ compression: '" << filename << "'";
        witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
        finalCode =  -6;
        return finalCode;
      }
      if ((bit_7 ^ bit_6) == 0) {
        ss.str("");
        ss << "LAS file is not compressed: '" << filename << "'";
        witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
        finalCode =  -7;
        return finalCode;
      }
    }
    inStrm->seekg(0);
    try {
      lazFileReader = unique_ptr<laszip::io::reader::file>(new laszip::io::reader::file(*inStrm));
      // laszip::io::reader::file f(file);
    } catch(const std::exception& e) {
      ss.str("");
      ss << "failed to create LAZ reader for this file: " << e.what();
      witnessString(actions::ACT_OPEN_ERROR, ss.str().c_str());
      finalCode = -3;
      return finalCode;
    }
    h = lazFileReader->get_header();
    auto &sc = lazFileReader->get_schema();
    schemaFormat = sc.format();
    {
      string sys_id, gen_soft;
      char cbuf[33];
      strncpy(cbuf, h.system_identifier, 32);
      sys_id.assign(cbuf);
      strncpy(cbuf, h.generating_software, 32);
      gen_soft.assign(cbuf);
      // See
      // http://www.asprs.org/wp-content/uploads/2019/07/LAS_1_4_r15.pdf
      // https://en.wikipedia.org/wiki/Universally_unique_identifier#Format
      ss.str("");
      for(int i = 0; i < 16; i++) {
        ss << setfill('0') << setw(2) << hex << (int)h.guid[i];
      }
      string guid = ss.str();
      ss.str("");
      ss << dec << fixed << setprecision(6)
      << "version: " << (int)h.version.major << "." << (int)h.version.minor << endl
      << "system_identifier: " << sys_id << endl
      << "generating_software: " << gen_soft << endl
      << "guid: " << guid << endl
      << "creation_day: " << h.creation.day << endl
      << "creation_year: " << h.creation.year << endl
      << "header_size: " << h.header_size << endl
      << "point_offset: " << h.point_offset << endl
      << "vlr_count: " << h.vlr_count << endl
      << "point_format_id: " << (int)h.point_format_id << endl
      << "schema_format: " << schemaFormat << endl
      << "point_record_length: " << h.point_record_length << endl
      << "point_count: " << h.point_count << endl
      << "points_per_block: " << pointsPerBlock << endl
      << "offset_x: " << h.offset.x << endl
      << "offset_y: " << h.offset.y << endl
      << "offset_z: " << h.offset.z << endl
      << "x_min: " << h.minimum.x << endl
      << "y_min: " << h.minimum.y << endl
      << "z_min: " << h.minimum.z << endl
      << "x_max: " << h.maximum.x << endl
      << "y_max: " << h.maximum.y << endl
      << "z_max: " << h.maximum.z << endl
      << "file_name: " << filename << endl;
      string s = ss.str();
      witnessString(actions::ACT_HEADER, s.c_str());
    }
    pointCount = h.point_count;
    // point budget calculations...
    say("point budget is %ld\n", pointBudget);
    if (pointBudget > 0) {
      if (pointCount > pointBudget) {
        uint32_t excessPoints = pointCount - pointBudget;
        say("point budget excess is %ld\n", excessPoints);
        pointBudgetInterval = double(pointCount) / double(pointBudget);
        say("point budget interval is %g\n", pointBudgetInterval);
      }
    }

    say("LAS/LAZ header is good: point count is %ld\n", pointCount);
    witnessFloat(actions::ACT_POINT_COUNT, double(pointCount));
    // TODO: We would normally be wanting the caller to decide on the packing strategy.
    string y = gd.toCheapYaml();
    witnessString(actions::ACT_GEOM_DEF, y.c_str());
    say("YAML is '%s'\n", y.c_str());
    // TODO: only include required fields based on LAS format combinations of point10, gpstime, rgb (and in the future user-data, classification, extra-bytes VLR data, etc.)
    // TODO: configuration by caller to map LAS fields to caller given point geometry
    // absence of any of these common items in the gd will be -1 field index
    fx = gd.fieldIndexByName("x");
    fy = gd.fieldIndexByName("y");
    fz = gd.fieldIndexByName("z");
    ft = gd.fieldIndexByName("t");
    fr = gd.fieldIndexByName("r");
    fg = gd.fieldIndexByName("g");
    fb = gd.fieldIndexByName("b");
    fi = gd.fieldIndexByName("i");
    if(fx == -1 || fy == -1 || fz == -1) {
      // throw yikes("geometry definition for LAS must include at least x, y, and z fields");
      say("geometry definition for LAS must include at least x, y, and z fields");
      finalCode = 2;
      return finalCode;
    }
    // Packed data block with space for N repacked points of chosen packing format
    blockSizeBytes = pointsPerBlock * gd.pointStep;
    bp = (uint8_t *)malloc(blockSizeBytes);
    say("allocated %ld bytes\n", blockSizeBytes);
    start = common::tick();
  #ifdef CSVDUMP
    outCsv.open("lazdecode-dump.csv");
    outCsv << std::fixed << std::setprecision(4);
  #endif
    return 0;
  }

  EM_BOOL decodeBlock(double time, void* userData) {
    size_t blockPointCount = 0;
    for(int yield = 0; (pointId < pointCount) && (yield < blocksPerYield); pointId ++) {
      lazFileReader->readPoint(buf);
      laszip::formats::las::point10 p = laszip::formats::packers<laszip::formats::las::point10>::unpack(buf);
      size_t off = sizeof(laszip::formats::las::point10);
      // if(debug) cout << p.x << ", " << p.y << ", " << p.z;
      double x_val = (h.scale.x * (double)p.x);
      double y_val = (h.scale.y * (double)p.y);
      double z_val = (h.scale.z * (double)p.z);
      // if(debug) say("xyz = %.3f %.3f %.3f\n", x_val = y_val = z_val)
#ifdef CSVDUMP
      outCsv << x_val << "," << y_val << "," << z_val;
#endif
      // retain this point?
      bool retain = true;
      if (pointBudgetInterval != 0.0) {
        auto next = floor( (double(pointSampleId) * pointBudgetInterval) + (pointBudgetInterval / 2.0) );
        // let's keep point zero for timing later...
        if ( pointId && (pointId < uint32_t(next)) ) {
          retain = false;
        }
      }
      // point start index within block...
      uint32_t psi = blockPointCount * gd.pointStep;
      if (retain) {
        // TODO: this data injection should be generic
        *((float *)&bp[psi + gd.fields[fx].offset]) = (float)x_val;
        *((float *)&bp[psi + gd.fields[fy].offset]) = (float)y_val;
        *((float *)&bp[psi + gd.fields[fz].offset]) = (float)z_val;
      }
      // So here, if we asked for time but there's no times in the LAS then the times will be zero...
      double t_val = 0.0;
      if (schemaFormat == 1 || schemaFormat == 3) {
        laszip::formats::las::gpstime gt = laszip::formats::packers<laszip::formats::las::gpstime>::unpack(buf + off);
        off += sizeof(laszip::formats::las::gpstime);
        memcpy(&t_val, &gt.value, sizeof(double));
#ifdef CSVDUMP
        outCsv << "," << std::setprecision(8) << t_val << std::setprecision(4);
#endif
        if (pointId == 0) { timeZero = t_val; }
        if (pointId + 1 == pointCount) { timeFinal = t_val; }
        t_val -= timeZero;
      }
      if (retain && (ft >= 0)) { *((float *)&bp[psi + gd.fields[ft].offset]) = (float)t_val; }
      uint8_t rgb_r = 0;
      uint8_t rgb_g = 0;
      uint8_t rgb_b = 0;
      uint8_t rgb_i = 0;
      if (schemaFormat == 2 || schemaFormat == 3) {
        laszip::formats::las::rgb rgb = laszip::formats::packers<laszip::formats::las::rgb>::unpack(buf + off);
        off += sizeof(laszip::formats::las::rgb);
#ifdef CSVDUMP
        outCsv << "," << rgb.r << "," << rgb.g << "," << rgb.b << "," << p.intensity;
#endif
        if (!colourShift && ((rgb.r & 0xFF00) || (rgb.g & 0xFF00) || (rgb.g & 0xFF00))) {
          if (pointId) {
            string s = "Colour components are 16-bit - some early colours may be incorrect";
            witnessString(ACT_WARNING_MSG, s.c_str());
          }
          colourShift = 8;
        }
        rgb_r = (uint8_t)(rgb.r >> colourShift);
        rgb_g = (uint8_t)(rgb.g >> colourShift);
        rgb_b = (uint8_t)(rgb.b >> colourShift);
        if (!hasColour && (rgb_r || rgb_g || rgb_g)) { hasColour = true; }
      }
      rgb_i = (uint8_t)(p.intensity >> 8);
      if (!hasIntensity && rgb_i) { hasIntensity = true; }
      if (retain) {
        // TODO: this data injection should be generic
        if (fr >= 0) { *((uint8_t *)&bp[psi + gd.fields[fr].offset]) = rgb_r; }
        if (fg >= 0) { *((uint8_t *)&bp[psi + gd.fields[fg].offset]) = rgb_g; }
        if (fb >= 0) { *((uint8_t *)&bp[psi + gd.fields[fb].offset]) = rgb_b; }
        if (fi >= 0) { *((uint8_t *)&bp[psi + gd.fields[fi].offset]) = rgb_i; }
      }
#ifdef CSVDUMP
      outCsv << "\n";
#endif
      // if(debug) cout << endl;
      // TODO deal with extra bytes, waveform data, god knows what else
      // If point size is larger than the expected minimum then there are extra bytes.
      // Extra bytes _may_ be described in VLR
      // When block is complete (either full or last point) then publish it
      if (retain) {
        pointSampleId++;
        blockPointCount++;
      }
      if ( (pointId + 1 == pointCount) || (blockPointCount == pointsPerBlock) ) {
        if (firstBlockCheck) {
          if ( (schemaFormat == 2 || schemaFormat == 3) && !hasColour) {
            string s = "LAZ_RGB_CLAIM_POSSIBLY_FALSE";
            witnessString(ACT_WARNING_MSG, s.c_str());
          }
          firstBlockCheck = false;
        }
        // if(debug) cout << "publish block" << endl;
        // say("publish block of %ld  pointSampleId = %ld  pointId = %ld\n", blockPointCount, pointSampleId, pointId);
        uint32_t len = blockPointCount * gd.pointStep;
        uint32_t res = witnessPointer(actions::ACT_BLOCK_ADDRESS, bp, len);
        if (res) {
          cancelLazLoad = res;
          break;
        }
        yield++;
        blockPointCount = 0;
      }
    }
    if(cancelLazLoad || pointId >= pointCount) {
      free(bp);
      inStrm->close();
      auto p = lazFileReader.release();
      delete p;
      float t = common::since(start);
      float pps = (float)pointCount / t;
      if(cancelLazLoad) {
        say("WARNING: cancelLazLoad triggered (%ld).\n", cancelLazLoad);
        witnessFloat(actions::ACT_POINT_COUNT, double(pointCount));
        timeFinal = timeZero
      }
      say("Read through %ld points in %.3f seconds = %.3f pps.\n", pointCount, t, pps);
      {
        stringstream ss;
        ss << dec << fixed << setprecision(9);
        ss << "timeZero" << ": " << timeZero << endl << "timeFinal" << ": " << timeFinal << endl << "hasIntensity" << ": " << hasIntensity << endl << "hasColour" << ": " << hasColour << endl << "pointSampleId" << ": " << pointSampleId << endl;
        witnessString(actions::ACT_TIME_RANGES_ETC, ss.str().c_str());
      }
      witnessFloat(actions::ACT_FINAL_DURATION, double(t));
      finalCode = cancelLazLoad;
      witnessFloat(actions::ACT_FINAL_CODE, double(finalCode));
#ifdef CSVDUMP
      outCsv.close();
#endif
      return EM_FALSE;
    }
    return EM_TRUE;
  }
};

CloudFileDecoder cloudFileDec;

EM_BOOL one_iter(double time, void* userData) {
  return cloudFileDec.decodeBlock(time, userData);
}

int main(int argc, const char *argv[]) {
  say("LazDecode main: argc = %d\n", argc);
  for (int i = 0; i < argc; i++) {
    say("  argv[%d] = '%s'\n", i, argv[i]);
  }
  if(argc != 6) {
    witnessString(actions::ACT_OPEN_ERROR, "bad args\n");
    witnessFloat(actions::ACT_FINAL_CODE, double(-1));
    return -1;
  }
  int err = cloudFileDec.openLaz(argv[1], atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));
  if(err != 0) {
    witnessFloat(actions::ACT_FINAL_CODE, double(err));
    return err;
  }
#ifdef __EMSCRIPTEN__
  emscripten_request_animation_frame_loop(one_iter, 0);
#else
  while (1) {
    if(!one_iter(0, (void *)0)) {
      break;
    }
  }
#endif
  return 0;
}


#ifndef __EMSCRIPTEN__
extern "C" {
  uint32_t witnessFloat(int action, double v1) {
    std::cout << "witness: action = " << action << ", val = " << v1 << std::endl;
  }
  uint32_t witnessString(int action, const char* text) {
    std::cout << "witness: action = " << action << ", text = " << text << std::endl;
  }
  uint32_t witnessPointer(int action, const uint8_t* ptr, uint32_t len) {
    std::cout << "witness: action = " << action << ", pointer = " << (size_t)ptr << ", len = " << len << std::endl;
  }
}
#endif

