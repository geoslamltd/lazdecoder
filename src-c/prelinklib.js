mergeInto(LibraryManager.library, {
    witnessString: function(action, ptr) {
        const s = AsciiToString(ptr)
        // console.log('witnessString: action '+action+ " val " + s)
        if (Module.superDynamicCallback1 instanceof Function) {
            return Module.superDynamicCallback1(action, s)
        }
    },
    witnessFloat: function(action, val) {
        // console.log('witnessFloat: action '+action+ " val " + val)
        if (Module.superDynamicCallback1 instanceof Function) {
            return Module.superDynamicCallback1(action, val)
        }
    },
    witnessPointer: function(action, ptr, len) {
        // console.log('witnessPointer: action '+action+ " ptr " + ptr + " len " + len)
        if (Module.superDynamicCallback1 instanceof Function) {
            return Module.superDynamicCallback1(action, ptr, len)
        }
    }
});
