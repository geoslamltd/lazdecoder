@ECHO OFF

REM NB: requires a Win32 GNU make exe in your path - the one used successfully is https://raw.githubusercontent.com/schellingb/ZillaLib/master/Tools/make.exe
REM Set this to the path of your emscripten SDK
SET MY_EMSDK=C:\ap\emsdk
SET PATH=C:\ap\MinGW\bin;%PATH%
REM Set this to the cmake DCMAKE_BUILD_TYPE: Debug, RelWithDebInfo, Release, etc.
SET MY_BUILD_TYPE=Release

cd /D "%~dp0"
IF NOT DEFINED EMSDK (
    ECHO SETUP EMSDK...
    cd %MY_EMSDK%
    call emsdk_env.bat
)
cd /D "%~dp0"

cmake -E make_directory build
cd build
call emcmake cmake .. -DCMAKE_BUILD_TYPE=%MY_BUILD_TYPE% -G"MinGW Makefiles" -DCMAKE_TOOLCHAIN_FILE=%EMSDK%/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake
REM you might want to do: cmake --build . -v
cmake --build .
if %errorlevel% equ 0 goto :good
ECHO FAILED!!!!!!!
exit
:good
cd ..

echo.
echo install stage - should be done in cmake but kinda complicated right now
echo.

cmake -E make_directory ../dist
COPY /Y /B head.txt + build\lazdecode.js ..\dist\lazdecode.js
REM NB: alter name for webpack loader
xcopy /y build\lazdecode.wasm ..\dist\lazdecode.bin
