/**
 * LazDecoder - Point cloud file decoding for LAZ format.
 *
 * Copyright (C) 2020 Michael Erskine (michael.erskine@geoslam.com)
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef POINTFIELD_HPP_
#define POINTFIELD_HPP_

#include <cinttypes>
#include <string>
#include <vector>
#include <cctype>
#include <string>
#include <algorithm>
/**
 * PointField definitions in the Point Cloud Library (PCL) style (https://pointclouds.org/)
 * Suitable for use with ROS PointCloud2 messages.
 * These definitions translate easily to LAS, PLY, and many other point formats.
 * Some things to note: -
 * - is_bigendian field is usually always false
 * - the data types are otherwise completely unambiguous
 * - pointStep is implied by the field definitions unless there is a padding gap required at the end
 * - field alignment is usually pretty important for performance
 * - field count has limited support in lots of software
 * - packing of bit fields is outside the scope of this format
 */
enum PointFieldType
{
PFT_None,
PFT_Int8,
PFT_UInt8,
PFT_Int16,
PFT_UInt16,
PFT_Int32,
PFT_UInt32,
PFT_Float32,
PFT_Float64,
PFT_Count
};

static const unsigned PointFieldWidth[PointFieldType::PFT_Count] = { 0, 1, 1, 2, 2, 4, 4, 4, 8 };

static const char *PointFieldTypeName[PointFieldType::PFT_Count] = {
    "UNKNOWN", "INT8", "UINT8", "INT16", "UINT16", "INT32", "UINT32", "FLOAT32", "FLOAT64",
};

/// Defines a data field in a buffer
struct PointField
{
    std::string name;
    uint32_t offset = 0;
    uint8_t dataType = 0;
    uint32_t count = 0;

    static const char *datatypeToString(uint8_t val) {
        if (val < PFT_Count) {
            return PointFieldTypeName[val];
        }
        return PointFieldTypeName[0];
    }

    static uint8_t datatypeFromString(const std::string &str) {
        for (uint8_t i = 0; i < PFT_Count; i++) {
            if (str.compare(PointFieldTypeName[i]) == 0) { return i; }
        }
        return 0;
    }

    static bool diff(const PointField &a, const PointField &b) {
        if (a.name != b.name) { return true; }
        if (a.offset != b.offset) { return true; }
        if (a.dataType != b.dataType) { return true; }
        if (a.count != b.count) { return true; }
        return false;
    }

    // Data field sizes corresponding to PointFieldType.
    static void add(std::vector<PointField> &v, const std::string &name, uint8_t dataType, uint32_t count,
                        uint32_t &offset)
    {
        PointField pf;
        pf.name = name;
        pf.dataType = dataType;
        pf.count = count;
        pf.offset = offset;
        v.push_back(pf);
        offset += (PointFieldWidth[dataType] * count);
    }

};

struct GeomDef {
    std::string geometryType;
    uint32_t pointStep = 0;
    bool is_bigendian = false;
    std::vector<PointField> fields;
    GeomDef(){};
    GeomDef(const std::string &tname, const std::string &fieldText) : geometryType(tname) { fieldsFromString(fieldText); }
    /**
     * NB: Assumes all fields have a count of 1
     */
    uint32_t fixOffsets() {
        uint32_t off = 0;
        for(auto &f: fields) {
            f.count = 1;
            f.offset = off;
            off += PointFieldWidth[f.dataType];
        }
        return off;
    }
    /**
     * Populate fields from a simplified text description,
     * e.g. "x: FLOAT32, y: FLOAT32, z: FLOAT32, time: UINT64"
     * Does nothing smart other than set the offsets. Only supports field count of 1. No gaps in data. Fields are in correct order.
     */
    uint32_t fieldsFromString(const std::string &s) {
        fields.clear();
        auto v = splitChar(',', s);
        for(auto &s: v) {
            auto kv = splitChar(':', s);
            if(kv.size() != 2) continue;
            auto k = trim(kv[0]);
            auto v = trim(kv[1]);
            PointField pf;
            pf.name = k;
            pf.dataType = PointField::datatypeFromString(v);
            pf.count = 1;
            fields.push_back(pf);
        }
        pointStep = fixOffsets();
        return pointStep;
    }
    uint32_t fieldIndexByName(const char *nom) {
        for (size_t i = 0; i < fields.size(); i++) {
            if(fields[i].name == nom) return i;
        }
        return -1;
    }
    std::string toCheapYaml() {
        std::stringstream ss;
        ss << "geometryType: " << geometryType << std::endl;
        ss << "is_bigendian: " << (is_bigendian ? "true" : "false") << std::endl;
        ss << "pointStep: " << pointStep << std::endl;
        ss << "fields: [" << std::endl;
        for(auto &f: fields) {
            ss << "    { name: " << f.name << ", offset: " << f.offset << ", datatype: " << PointField::datatypeToString(f.dataType) << ", count: " << f.count << " }," << std::endl;
        }
        ss << "]" << std::endl;
        return ss.str();
    }
    static inline std::string trim(const std::string &s)
    {
        auto wsfront=std::find_if_not(s.begin(),s.end(),[](int c){return std::isspace(c);});
        auto wsback=std::find_if_not(s.rbegin(),s.rend(),[](int c){return std::isspace(c);}).base();
        return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
    }
    static std::vector<std::string> splitChar(const char sep, const std::string &s) {
        std::vector<std::string> v;
        for(size_t p=0, q=0; p!=std::string::npos; p=q) {
            std::string ss = s.substr(p+(p!=0), (q=s.find(sep, p+1))-p-(p!=0));
            v.push_back(ss);
        }
        return v;
    }

};

#endif  // POINTFIELD_HPP_
